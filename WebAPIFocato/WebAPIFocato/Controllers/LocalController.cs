using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPIFocato.Models;

namespace WebAPIFocato.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocalController : ControllerBase
    {
        private readonly focatodbContext _context;

        public LocalController(focatodbContext context)
        {
            _context = context;
        }

        // GET: api/Local
        [HttpGet]
        public IEnumerable<Local> GetLocal()
        {
            return _context.Local;
        }

        // GET: api/Local/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLocal([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var local = await _context.Local.FindAsync(id);

            if (local == null)
            {
                return NotFound();
            }

            return Ok(local);
        }

        // PUT: api/Local/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLocal([FromRoute] int id, [FromBody] Local local)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != local.Id)
            {
                return BadRequest();
            }

            _context.Entry(local).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LocalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(local);
        }

        // POST: api/Local
        [HttpPost]
        public async Task<IActionResult> PostLocal([FromBody] Local local)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Local.Add(local);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LocalExists(local.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetLocal", new { id = local.Id }, local);
        }

        // DELETE: api/Local/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLocal([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var local = await _context.Local.FindAsync(id);
            if (local == null)
            {
                return NotFound();
            }

            _context.Local.Remove(local);
            await _context.SaveChangesAsync();

            return Ok(local);
        }

        private bool LocalExists(int id)
        {
            return _context.Local.Any(e => e.Id == id);
        }
    }
}
