﻿using System;
using System.Collections.Generic;

namespace WebAPIFocato.Models
{
    public partial class Local
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public bool? Ativo { get; set; }
    }
}
