﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebAPIFocato.Models
{
    public class focatodbContext : DbContext
    {
        public focatodbContext()
        {
        }

        public focatodbContext(DbContextOptions<focatodbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Local> Local { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Server=localhost;Database=focatodb;Port=5432;User Id=postgres;Password=admin;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Local>(entity =>
            {
                entity.ToTable("local");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Ativo).HasColumnName("ativo");

                entity.Property(e => e.Nome)
                    .HasColumnName("nome")
                    .HasMaxLength(100);
            });
        }
    }
}
