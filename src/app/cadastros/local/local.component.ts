import { Component, OnInit, ViewChild } from '@angular/core';
import { Local } from './models/local';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { LocalService } from './local.service';

@Component({
  selector: 'app-local',
  templateUrl: './local.component.html',
  styleUrls: ['./local.component.css']
})
export class LocalComponent implements OnInit {
  displayedColumns: string[] = ['actionsColumn','codigo', 'nome'];
  local: Local;
  teste: any;
  locais: any;
  dataSource: any;
  edit: boolean;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private localService: LocalService) { }

  ngOnInit() {
    this.local = new Local();
    this.locais = new Array<Local>();
    this.listAll();
  }

  listAll(){
    this.localService.findAll().subscribe(response => {
      if (response)
        this.loadTable(response);
    }, error => {
      console.log(error);
    });
  }

  loadTable(locais: any){
    this.dataSource = new MatTableDataSource<Local>(locais);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  salvar(){
    this.localService.save(this.local).subscribe(response => {
      if (response){
        alert('Salvou!!!!');
        this.listAll();
      }
    }, error => {
      console.log(error);
    });
    this.local = new Local();
  }

  excluir(localId: number){
    this.localService.remove(localId).subscribe(response => {
      if (response)
        this.listAll();
    }, error => {
      console.log(error);
    });
  }

  markEdit(local: any){
    this.local = local;
    this.edit = true;
  }

  atualizar(){
    this.localService.update(this.local).subscribe(response => {
      if (response){
        alert('Atualizou!!!!');
        this.listAll();
        this.edit = false;
        this.local = new Local();
      }        
    }, error => {
      console.log(error);
    });
  }

}
