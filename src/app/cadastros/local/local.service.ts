import { Injectable } from '@angular/core';
import { BaseService } from '../../shared/base.service';
import { HttpClient } from '@angular/common/http';
import { Local } from './models/local';
import { Observable } from 'rxjs-compat';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocalService extends BaseService{

  constructor(private http: HttpClient) { 
    super();
  }

  save(local: Local): Observable<any>{
    return this.http.post(environment.urlAPI+"local/", local)
    .catch((error: any) => Observable.throw(error));
  }

  update(local: Local): Observable<any>{
    return this.http.put(environment.urlAPI+"local/"+local.id, local)
    .catch((error: any) => Observable.throw(error));
  }
  
  findAll(): Observable<any>{
    return this.http.get(environment.urlAPI+"local/")
    .catch((error: any) => Observable.throw(error));
  }

  remove(id: number): Observable<any> {
    return this.http.delete(environment.urlAPI+"local/"+id)
    .catch((error: any) => Observable.throw(error));
  }

}
